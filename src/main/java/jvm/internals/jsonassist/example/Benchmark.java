package jvm.internals.jsonassist.example;

import com.google.gson.Gson;
import jvm.internals.jsonassist.Jitson;

import java.util.function.Function;

public class Benchmark {
    public static void main(String[] args) throws Exception {
        Gson googleConverter = new Gson();
        Jitson customConverter = new Jitson();

        AllPublic allPublic = new AllPublic();
        AllPrivate allPrivate = new AllPrivate();
        ClassWithNoFields classWithNoFields = new ClassWithNoFields();
        MixedPrivateAndPublic mixedPrivateAndPublic = new MixedPrivateAndPublic();
        OneLayerOfClasses oneLayerOfClasses = new OneLayerOfClasses();
        TwoLayersOfClasses twoLayersOfClasses = new TwoLayersOfClasses();

        compare(allPublic, customConverter, googleConverter);
        compare(allPrivate, customConverter, googleConverter);
        compare(classWithNoFields, customConverter, googleConverter);
        compare(mixedPrivateAndPublic, customConverter, googleConverter);
        compare(oneLayerOfClasses, customConverter, googleConverter);
        compare(twoLayersOfClasses, customConverter, googleConverter);

    }

    private static void compare(Object o, Jitson j, Gson g) {
        long jitTime = measure(j::toJson, o);
        long googleTime = measure(g::toJson, o);
        System.out.println("\n" + o.getClass().getSimpleName() + " object: jit = " + jitTime + " google = " + googleTime
				+ " ratio = " + (double) googleTime / jitTime);
        printOutConversionResults(j::toJson, o);
        printOutConversionResults(g::toJson, o);
    }

    private static long measure(Function<Object, String> converter, Object o) {
        long t1 = System.currentTimeMillis();
        int times = 500000;
        for (int i = 0; i < times; i++) {
            converter.apply(o);
        }
        return System.currentTimeMillis() - t1;
    }

    private static void printOutConversionResults(Function<Object, String> converter, Object o) {
        System.out.println(converter.apply(o));
    }

}
