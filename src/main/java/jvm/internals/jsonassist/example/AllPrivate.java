package jvm.internals.jsonassist.example;

public class AllPrivate {
    private String stringField = "abc";
    private int integerField = 1;

    public String getStringField() {
        return stringField;
    }

    public int getIntegerField() {
        return integerField;
    }
}
