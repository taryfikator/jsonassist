package jvm.internals.jsonassist.example;

public class MixedPrivateAndPublic {
    private int privateInt = 10;
    public String publicString = "abc";

    public int getPrivateInt() {
        return privateInt;
    }
}
