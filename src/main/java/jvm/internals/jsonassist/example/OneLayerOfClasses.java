package jvm.internals.jsonassist.example;

public class OneLayerOfClasses {
    public AllPublic allPublic = new AllPublic();
    private AllPrivate allPrivate = new AllPrivate();

    public AllPrivate getAllPrivate() {
        return allPrivate;
    }
}
