package jvm.internals.jsonassist;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewMethod;
import javassist.NotFoundException;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Jitson {

    private ClassPool pool;
    private Map<Class<?>, JsonConverter> cache;

    public Jitson() {
        pool = ClassPool.getDefault();
        cache = new ConcurrentHashMap<>();
    }

    public String toJson(Object o) {
        try {
            if (!cache.containsKey(o.getClass())) {
                cache.put(o.getClass(), getConverter(o.getClass()));
            }
            return cache.get(o.getClass()).toJson(o);
        } catch (Exception e) {
            return "";
        }
    }

    private JsonConverter getConverter(Class<?> cls)
            throws CannotCompileException, NotFoundException, InstantiationException, IllegalAccessException {

        // new class with a random name, as this name is not needed in any way
        CtClass converterClass = pool.makeClass(UUID.randomUUID().toString());
        converterClass.addMethod(CtNewMethod.make(getConverterMethodBody(cls), converterClass));
        converterClass.addMethod(CtNewMethod.make("public String toJson(Object o){return toJson((" + cls.getName() + ")o);}", converterClass));
        converterClass.setInterfaces(new CtClass[]{pool.get("jvm.internals.jsonassist.JsonConverter")});
        JsonConverter result = (JsonConverter) pool.toClass(converterClass).newInstance();

        // this allows us to save memory
        converterClass.detach();
        return result;
    }

    private String getConverterMethodBody(Class<?> cls) {
        StringBuilder sb = new StringBuilder("public String toJson(" + cls.getName() + " o) { ");
        String newLine = "String json = " + "\"{\" + " + formJson(cls, "o.") + " + \"}\"" + ";";
        sb.append(newLine);
        sb.append("return json;}");
        return sb.toString();
    }

    private String formJson(Class<?> cls, String startGettingValueFrom) {
        String publicPart = extractPublicFields(cls.getFields(), startGettingValueFrom);
        String privatePart = extractPrivateFields(cls, startGettingValueFrom);
        String result = "";
        if (publicPart.length() > 0 && privatePart.length() > 0) {
            result = publicPart + " + \",\" + " + privatePart;
        } else if (publicPart.length() > 0) {
            result = publicPart;
        } else if (privatePart.length() > 0) {
            result = privatePart;
        }
        return result;
    }


    private String extractPublicFields(Field[] fields, String startGettingValueFrom) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            sb.append(dealWithPublicField(startGettingValueFrom, fields[i]));
        }
        if (sb.length() > 0) {
            sb.delete(sb.length() - 9, sb.length() - 1);
        }
        return sb.toString();
    }

    private String extractPrivateFields(Class<?> cls, String startGettingValueFrom) {
        StringBuilder sb = new StringBuilder();
        try {
            for (PropertyDescriptor pd : Introspector.getBeanInfo(cls).getPropertyDescriptors()) {
                sb.append(dealWithPrivateField(pd, startGettingValueFrom));
            }
            if (sb.length() > 0) {
                sb.delete(sb.length() - 9, sb.length() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private String dealWithPublicField(String startGettingValueFrom, Object object) {
        StringBuilder sb = new StringBuilder();
        Field field = (Field) object;
        if (field.getType().isPrimitive()) {
            sb.append("\"\\\"" + field.getName() + "\\\":\" + " + startGettingValueFrom + field.getName());
            sb.append(" + \",\" + ");
        } else if (field.getType().isAssignableFrom(String.class)) {
            sb.append("\"\\\"" + field.getName() + "\\\":\\\"\" + " + startGettingValueFrom + field.getName() + " + \"\\\"\"");
            sb.append(" + \",\" + ");
        } else {
            sb.append("\"\\\"" + field.getName() + "\\\":\" + \"{\" + ");
            sb.append(formJson(field.getType(), startGettingValueFrom + field.getName() + "."));
            sb.append(" + \"}\"");
            sb.append(" + \",\" + ");
        }
        return sb.toString();
    }

    private String dealWithPrivateField(PropertyDescriptor pd, String startGettingValueFrom) {
        StringBuilder sb = new StringBuilder();
        if (pd.getReadMethod() != null && !"class".equals(pd.getName())) {
            if (pd.getPropertyType().isPrimitive()) {
                sb.append("\"\\\"" + pd.getName() + "\\\":\" + " + startGettingValueFrom + pd.getReadMethod().getName() + "()");
                sb.append(" + \",\" + ");
            } else if (pd.getPropertyType().isAssignableFrom(String.class)) {
                sb.append("\"\\\"" + pd.getName() + "\\\":\\\"\" + " + startGettingValueFrom + pd.getReadMethod().getName() + "()" + " + \"\\\"\"");
                sb.append(" + \",\" + ");
            } else {
                sb.append("\"\\\"" + pd.getName() + "\\\":\" + \"{\" + ");
                sb.append(formJson(pd.getPropertyType(), startGettingValueFrom + pd.getReadMethod().getName() + "()" + "."));
                sb.append(" + \"}\"");
                sb.append(" + \",\" + ");
            }
        }
        return sb.toString();
    }
}
