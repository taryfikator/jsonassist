JITSon aka Jsonassist
=====================

Konwerter obsługuje: pola publiczne, pola niepubliczne z getterami, typy proste oraz obiekty.
Składowe będące obiektami mogą być zagnieżdżane (przykład - klasa TwoLayersOfClasses), 
ale nie obsługuję klas, które zawierają same siebie jako składowe.
Obsługiwane pola mogą występować w różnych kombinacjach, co próbuję pokazać w testach.

Aplikacja bezpieczna wielowątkowo - użyłam ConcurrentHashMap.
Testy zwracają porównanie czasów oraz wyniki generowane przez GSONa i testowany konwerter.